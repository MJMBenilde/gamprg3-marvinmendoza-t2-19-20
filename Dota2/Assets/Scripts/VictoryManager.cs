﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
public class VictoryManager : MonoBehaviour
{
    bool victory;
    public Text victoryText;
    public UnityEvent victoryCleanup;
    string winnerName;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void redLoses(string name)
    {
        winnerName = name;
        StartCleanup();
    }

    public void blueLoses(string name)
    {
        winnerName = name;
        StartCleanup();
    }
    void announcedWinner()
    {
        victoryText.text = winnerName + " WINS!";
    }

    //ends the game by
    public void StartCleanup()
    {
        victoryCleanup.Invoke(); //activate other stuff to clean up
        EnableGameObjects(victoryText.gameObject);
        announcedWinner();
    }

    //disables GameObjects
    public void DisableGameObjects(GameObject target)
    {
        target.SetActive(false);
    }

    public void EnableGameObjects(GameObject target)
    {
        target.SetActive(true);
    }
}
