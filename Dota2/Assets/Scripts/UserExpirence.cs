﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserExpirence : MonoBehaviour
{
    public int level = 1;
    public int currentEXP = 0;
    public int nextEXP = 0; //UI code
    public float respawnTime = 6;
    public levelList[] userLevelList;
    public ExpirenceController expController;

    public UserExpirence attacker;
    public ExpSender attackerb;

    //stats raising upon level up - note: can't find a specific information for this but it look like a fixed rate
    public int health;
    public int mana;

    public Stats userStats;
    public Health userHealthStats;
    public Mana userManaStats;
    // Start is called before the first frame update
    void Start()
    {
        userLevelList = expController.levelLists; //setup expirence list
        userStats = this.gameObject.GetComponent<Stats>();
        userHealthStats = this.gameObject.GetComponent<Health>();
        userManaStats = this.gameObject.GetComponent<Mana>();
        SetupNextEXP();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //call this every kill
    public void gainEXP(int expirence)
    {
        currentEXP += expirence;

        //check if level up
        if (currentEXP >= userLevelList[level + 1].requiredXP)
        {
            levelUP();
        }
    }

    //for hero to hero exchange
    public int sendEXP()
    {
        return userLevelList[level].xpBounty;
    }

    public void levelUP()
    {
        //repeat the process until reached the new level
        while (currentEXP >= userLevelList[level + 1].requiredXP)
        {
            level++;
            respawnTime = userLevelList[level].respawnTime;
            nextEXP = userLevelList[level + 1].requiredXP;
            raiseStats();
        }
    }

    public float getRespawnTime()
    {
        return respawnTime;
    }

    //raise stats every Level up
    public void raiseStats()
    {
        userHealthStats.raisesHealth(health);
        userManaStats.RaisesMana(mana);
    }

    public int getLevelBounty()
    {
        return userLevelList[level].xpBounty;
    }

    public float getNormalizedEXP()
    {
        return currentEXP / nextEXP;
    }

    public void SetupNextEXP()
    {
        nextEXP = userLevelList[level + 1].requiredXP;
    }
}
