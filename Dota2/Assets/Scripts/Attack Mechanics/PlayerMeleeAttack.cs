﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMeleeAttack : MonoBehaviour
{
    public bool withinAttackRange;
    public GameObject target;

    public float attackSpeed; //attack speed
    public float timer;
    public int damage;
    public Stats stats;

    void Start()
    {
        stats = this.gameObject.GetComponent<Stats>();
        
    }
    void Update()
    {
        timer-= Time.deltaTime;

        if (withinAttackRange == true && timer <= 0  && this.GetComponent<CharacterMovement>().attacking == true)
        {
            target.GetComponent<Health>().Damage(stats.damage);
            timer = attackSpeed;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == target)
        {
            withinAttackRange = true;
        }
    }
    //if next target is within range
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject == target)
        {
            withinAttackRange = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == target)
        {
            withinAttackRange = false;
            this.gameObject.GetComponent<CharacterMovement>().targetEscaping();
        }
    }

    //set to negative to decrease
    public void modifyAttackSpeed(float amount)
    {
        attackSpeed += amount;
    }

    public void UpdateAttackSpeed()
    {
        attackSpeed = stats.attackSpeed;
    }
}
