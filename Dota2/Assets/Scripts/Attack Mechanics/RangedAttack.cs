﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedAttack : AttackComponent
{
    /// <summary>
    /// The projectile to instantiate on attack
    /// </summary>
    public GameObject Projectile;
    public float ProjectileSpeed;

    private List<Projectile> activeProjectiles;

    private void Start()
    {
        activeProjectiles = new List<Projectile>();
    }

    protected override void StartAttack()
    {
        base.StartAttack();

        //Create a new projectile
        GameObject newGO = Instantiate(Projectile, this.transform);

        //Add projectile component if the projectile doesn't have one
        if (newGO.GetComponent<Projectile>() == null) newGO.AddComponent<Projectile>();

        //Initialize the values of the projectile
        Projectile temp = newGO.GetComponent<Projectile>();
        temp.SetValues(target.gameObject, this.gameObject, ProjectileSpeed, Damage);
        temp.OnProjectileHit.AddListener(OnTargetHit);

        activeProjectiles.Add(temp);
    }

    /// <summary>
    /// gets called if an ative projectile collides with the target
    /// </summary>
    private void OnTargetHit()
    {
        //Debug.Log(gameObject.name + " damaged " + target);
    }
}
