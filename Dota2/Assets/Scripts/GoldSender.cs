﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldSender : MonoBehaviour
{
    public GameObject target;
    public int goldToSend;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayerGold()
    {
        if (this.gameObject.GetComponent<UserExpirence>() == null) return;
        goldToSend = 110 + (this.gameObject.GetComponent<UserExpirence>().level * 8);

        return;
    }
    public void SendGold()
    {
        if (target.gameObject.GetComponent<Gold>() == null) return;
            target.gameObject.GetComponent<Gold>().gainGold(goldToSend);
            return;
    }
}
