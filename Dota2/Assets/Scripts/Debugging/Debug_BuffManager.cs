﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debug_BuffManager : MonoBehaviour
{
    public BuffReceiver target;
    public Buff[] buffs = new Buff[3];

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q)) target.ApplyNewBuff(Instantiate(buffs[0]));
        if (Input.GetKeyDown(KeyCode.W)) target.ApplyNewBuff(Instantiate(buffs[1]));
        if (Input.GetKeyDown(KeyCode.E)) target.ApplyNewBuff(Instantiate(buffs[2]));
    }
}
