﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum SpeedSelection
{
    slow,
    normal,
    fast,
    veryFast
}

public class DebugSystem : MonoBehaviour
{

    public Text speedUIText;

    public GameObject debugUIObject;
    public bool debugUIActivate;
    public float speedControl;

    public SpeedSelection setSpeed;

    public GameObject[] redTowerTop;
    public GameObject[] redTowerMid;
    public GameObject[] redTowerBot;
    public GameObject[] blueTowerTop;
    public GameObject[] blueTowerMid;
    public GameObject[] blueTowerBot;

    public GameObject[] redTowerfountain;
    public GameObject[] blueTowerfountain;

    //public GameObject[] redBarracks;
    //public GameObject[] blueBarracks;

    public GameObject[] HeroList;

    public PlayerManager playerManager;

    public ExpirenceController expController;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        speedUIText.text = "Speed: " + setSpeed.ToString();
    }

    public void DecreaseSpeed()
    {
        switch (setSpeed)
        {
            case SpeedSelection.slow:
                Debug.Log("Cannot decrease Speed");
                break;
            case SpeedSelection.normal:
                setSpeed = SpeedSelection.slow;
                break;
            case SpeedSelection.fast:
                setSpeed = SpeedSelection.normal;
                break;
            case SpeedSelection.veryFast:
                setSpeed = SpeedSelection.fast;
                break;
            default:
                break;
        }

        ChangeSpeed();
    }

    public void IncreaseSpeed()
    {
        switch (setSpeed)
        {
            case SpeedSelection.slow:
                setSpeed = SpeedSelection.normal;
                break;
            case SpeedSelection.normal:
                setSpeed = SpeedSelection.fast;
                break;
            case SpeedSelection.fast:
                setSpeed = SpeedSelection.veryFast;
                break;
            case SpeedSelection.veryFast:
                Debug.Log("Cannot increase Speed");
                break;
            default:
                break;
        }

        ChangeSpeed();
    }

    public void DestroyTower(GameObject tower)
    {
        tower.GetComponent<Health>().Damage(999999);
    }

    void ChangeSpeed()
    {

        switch (setSpeed)
        {
            case SpeedSelection.slow:
                speedControl = 0.25f;
                break;
            case SpeedSelection.normal:
                speedControl = 1.0f;
                break;
            case SpeedSelection.fast:
                speedControl = 2.0f;
                break;
            case SpeedSelection.veryFast:
                speedControl = 4.0f;
                break;
            default:
                break;
        }
        Time.timeScale = speedControl;
    }

    public void EnableDebugUI()
    {
        if (debugUIActivate == true)
        {
            debugUIActivate = false;
        }
        else
        {
            debugUIActivate = true;
        }
        debugUIObject.SetActive(debugUIActivate);
    }

    public void destroyRedTop()
    {
        for (int i = 0; i < redTowerTop.Length; i++)
        {
            if (redTowerTop[i] != null)
            {
                Debug.Log(redTowerTop[i].name + "is destroyed!");
                redTowerTop[i].GetComponent<Health>().Damage(999999);
                break;
            }
        }
    }

    public void destroyRedBottom()
    {
        for (int i = 0; i < redTowerBot.Length; i++)
        {
            if (redTowerBot[i] != null)
            {
                Debug.Log(redTowerBot[i].name + "is destroyed!");
                redTowerBot[i].GetComponent<Health>().Damage(999999);
                break;
            }
        }
    }

    public void destroyRedMid()
    {
        for (int i = 0; i < redTowerMid.Length; i++)
        {
            if (redTowerMid[i] != null)
            {
                Debug.Log(redTowerMid[i].name + "is destroyed!");
                redTowerMid[i].GetComponent<Health>().Damage(999999);
                break;
            }
        }
    }

    public void destroyBlueTop()
    {
        for (int i = 0; i < blueTowerTop.Length; i++)
        {
            if (blueTowerTop[i] != null)
            {
                Debug.Log(blueTowerTop[i].name + "is destroyed!");
                blueTowerTop[i].GetComponent<Health>().Damage(999999);
                break;
            }
        }
    }

    public void destroyBlueBottom()
    {
        for (int i = 0; i < blueTowerBot.Length; i++)
        {
            if (blueTowerBot[i] != null)
            {
                Debug.Log(blueTowerBot[i].name + "is destroyed!");
                blueTowerBot[i].GetComponent<Health>().Damage(999999);
                break;
            }
        }
    }

    public void destroyBlueMid()
    {
        for (int i = 0; i < blueTowerMid.Length; i++)
        {
            if (blueTowerMid[i] != null)
            {
                Debug.Log(blueTowerMid[i].name + "is destroyed!");
                blueTowerMid[i].GetComponent<Health>().Damage(999999);
                break;
            }
        }
    }

    public void destroyBluefountainDefense()
    {
        for (int i = 0; i < blueTowerfountain.Length; i++)
        {
            if (blueTowerfountain[i] != null)
            {
                Debug.Log(blueTowerfountain[i].name + "is destroyed!");
                blueTowerfountain[i].GetComponent<Health>().Damage(999999);
            }

            else
            {
                Debug.Log("Fountain Tower Already Destroyed");
            }

        }
    }

    public void destroyRedfountainDefense()
    {
        for (int i = 0; i < redTowerfountain.Length; i++)
        {
            if (redTowerfountain[i] != null)
            {
                Debug.Log(redTowerfountain[i].name + "is destroyed!");
                redTowerfountain[i].GetComponent<Health>().Damage(999999);
            }

            else
            {
                Debug.Log("Fountain Tower Already Destroyed");
            }

        }
    }

    //note: realised I could just assign the item to a ID#
    public void DestroyRedBarrack(GameObject barrack)
    {
        if (barrack.gameObject != null)
        {
            Debug.Log(barrack.name + "is destroyed!");
            barrack.GetComponent<Health>().Damage(999999);
        }
        else
        {
            Debug.Log("Barracks Already Destroyed");
        }
    }
    public void DestroyBlueBarrack(GameObject barrack)
    {
        if (barrack.gameObject != null)
        {
            Debug.Log(barrack.name + "is destroyed!");
            barrack.GetComponent<Health>().Damage(999999999999999999);
        }
        else
        {
            Debug.Log("Barracks Already Destroyed");
        }
    }

    public void LevelUpHero(int heroID)
    {
        //get hero level
        int targetCurrentLevel = HeroList[heroID].GetComponent<UserExpirence>().level;
        //get exp requirement for leveling up 
        int expToGet = expController.levelLists[targetCurrentLevel + 1].requiredXP;
        //divide the amount based on target current exp
        int actualExpAmount = expToGet - HeroList[heroID].GetComponent<UserExpirence>().currentEXP;
        Debug.Log("DEBUGGER Exp released" + actualExpAmount);
        //send new EXP to player
        HeroList[heroID].GetComponent<UserExpirence>().gainEXP(actualExpAmount);
    }

    public void CommitMurder()
    {
        for (int i = 0; i < playerManager.radiantTeamList.Length; i++)
        {
            playerManager.radiantTeamList[i].GetComponent<Health>().Damage(9999999999);
        }

        for (int i = 0; i < playerManager.direTeamList.Length; i++)
        {
            playerManager.direTeamList[i].GetComponent<Health>().Damage(9999999999);
        }
        Debug.Log("you have use your god power to kill those who opposed you!");
    }

    public void CommitMildMurder()
    {

        for (int i = 0; i < playerManager.direTeamList.Length; i++)
        {
            playerManager.direTeamList[i].GetComponent<Health>().Damage(9999999999);
        }
        Debug.Log("you have use your god power to kill those who opposed you!");
    }

    public void DestroyAncient(GameObject ancient)
    {
        ancient.GetComponent<Health>().Damage(9999999999);
    }

}
