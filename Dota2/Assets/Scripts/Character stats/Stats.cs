﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour
{
    public float armor;
    public float magicResistance;
    public float evasion;

    public float healthRegenSpeed;
    public float manaRegenSpeed;

    public float attackSpeed;
    public float movementSpeed;

    public float damage;

    //raises components
    public void raiseArmor(int amount)
    {
        armor += amount;
    }

    public void raiseMagicResistance(int amount)
    {
        magicResistance += amount;
    }

    public void raiseEvasion(int amount)
    {
        evasion += amount;
    }

    public void raisehealthRegenSpeed(int amount)
    {
        healthRegenSpeed += amount;
    }

    public void raisemanaRegenSpeed(int amount)
    {
        manaRegenSpeed += amount;
    }

    public float armorDamageMultiplier()
    {
        float formula = 1.00f - ((0.052f) * armor / (0.9f + 0.048f * Mathf.Abs(armor)));
        Debug.Log("Armor Damage Multiplier" + formula);
        return formula;
    }
}
