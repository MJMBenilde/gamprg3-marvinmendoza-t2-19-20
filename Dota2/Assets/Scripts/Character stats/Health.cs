﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    public float CurrentHealth { get => health; }
    public float MaxHealth { get => maxHealth; }

    [SerializeField]
    private float health;
    [SerializeField]
    private float maxHealth;

    public bool invunable; //use this if the enemy/target cannot be killed

    public Stats userStats;

    public float dmgText;

    public float baseRegen;
    public float regeneration;
    public float timer;
    public float regenTimer;
    // Start is called before the first frame update

    public int deathAmount;
    void Start()
    {
        health = maxHealth;
        userStats = this.gameObject.GetComponent<Stats>();
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <=0)
        {
            healthRegen(0);
            timer = regenTimer;
        }
    }

    public void healthRegen(float additonalRegen)
    {
        float finalRegenAmount = baseRegen + additonalRegen;
        Heal(finalRegenAmount);
    }

    public void RevivePlayer()
    {
        health = maxHealth;
    }

    public void Damage(float amount)
    {
        if (invunable != true)
        {
            float damaged = amount * userStats.armorDamageMultiplier();
            health -= damaged;
            dmgText = damaged;
        }

        if (health <= 0 && this.gameObject.tag == "creep")
        {
            Death();
            Debug.Log("Creep death");
        }

        if (health <= 0 && this.gameObject.tag == "player")
        {
            Despawn();
            Debug.Log("Player death");

        }

    }
    public void Heal(float amount)
    {
        health += amount;
        if (health >= maxHealth)
        {
            health = maxHealth;
        }

    }

    //for creep,monster, etc... use
    public void Death()
    {
        //run expirence system

        this.gameObject.GetComponent<GoldSender>().SendGold();
        Destroy(this.gameObject);
    }

    public void SpawnerDestruction()
    {
        this.GetComponent<Spawner>().ifDestroyed.Invoke();
        Destroy(this.gameObject);
    }

    public void TowerDestruction()
    {
        this.gameObject.GetComponent<Tower>().removeVunalibilityOfNextTower.Invoke();
        this.gameObject.GetComponent<Tower>().preDestruction.Invoke();
        this.gameObject.transform.position = new Vector3(9999, 9999, 9999);
        Destroy(this.gameObject);
    }

    //for hero use
    public void Despawn()
    {
        deathAmount++;
        this.gameObject.GetComponent<Respawn>().beginDespawning();
    }

    public void ancientDestruction()
    {
        this.gameObject.GetComponent<Ancient>().destroyed();
        Destroy(this.gameObject);
    }

    public float GetNormalizedHealth()
    {
        return health / maxHealth;
    }

    public void raisesHealth(int newAmount)
    {
        maxHealth += newAmount;
        health = maxHealth;
    }
}
