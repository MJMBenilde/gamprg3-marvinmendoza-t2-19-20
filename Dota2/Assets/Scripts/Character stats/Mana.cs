﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mana : MonoBehaviour
{
    public float CurrentMana { get => mana; }
    public float MaxMana { get => maxMana; }

    [SerializeField]
    private float mana;
    [SerializeField]
    private float maxMana;

    public float baseRegen;
    public float timer;
    public float regenTimer;
    // Start is called before the first frame update
    void Start()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            manaRegen(0);
            timer = regenTimer;
        }
    }

    public void manaRegen(float additionalamount)
    {
        float finalRegenAmount = baseRegen + additionalamount;
        gainMana(finalRegenAmount);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void gainMana(float amount)
    {
        mana += amount;
        if (mana >= maxMana)
        {
            mana = maxMana;
        }
    }

    public void deductMana(float amount)
    {
        mana -= amount;
        if (mana < 0)
        {
            Debug.Log("Mana: mana is on negative numeric");
        }
    }

    public bool useMana(float requiredAmount)
    {
        if (mana >= requiredAmount)
        {
            deductMana(requiredAmount);
            return true;
        }
        else
        {
            return false;
        }
    }

    public float GetNormalizedMana()
    {
        return mana / maxMana;
    }

    public void RaisesMana(int amount)
    {
        maxMana += amount;
        mana = maxMana;
    }
}
