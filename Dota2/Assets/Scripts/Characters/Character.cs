﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public Fraction fraction;

    public Health health { get; private set; }
    public Mana mana { get; private set; }
    public Stats stats { get; private set; }
    public BuffReceiver buffReceiver { get; private set; }

    protected virtual void Start()
    {
        //get component references
        health = GetComponent<Health>();
        mana = GetComponent<Mana>();
        stats = GetComponent<Stats>();
        buffReceiver = GetComponent<BuffReceiver>();
    }
}
