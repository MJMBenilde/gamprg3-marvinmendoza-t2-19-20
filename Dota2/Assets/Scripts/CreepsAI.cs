﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum creepAIStatus
{
    moving,
    attacking
}
public class CreepsAI : MonoBehaviour
{
    public GameObject target; //current target
    public GameObject[] towers; //stores tower list
    public GameObject[] routes; //stores routes list
    public int nextTowerId = 0;
    public int nextRouteID = 0;
    public int threshold = 3;
    public bool towerMovementMode = false;
    creepAIStatus status;
    public NavMeshAgent creepsAgent;

    // Start is called before the first frame update
    void Start()
    {
        creepsAgent = this.GetComponent<NavMeshAgent>();
        creepsAgent.SetDestination(routes[0].transform.position);
        target = null;
    }

    // Update is called once per frame
    void Update()
    {
        //set target to the next tower in the list
        if (target == null && towerMovementMode == true)
        {
            status = creepAIStatus.moving;
            switchTower();
        }

        else if (towerMovementMode == false)
        {
            movingCreep();
        }

        //start attacking target
        if (status == creepAIStatus.attacking)
        {
            GetComponent<AttackComponent>().Attack(target.GetComponent<Health>());
        }

        //moving back to tower
        //if (status == creepAIStatus.moving)
        //{
        //    switchTower();
        //}
        //movingCreep();
    }

    void movingCreep()
    {
        Debug.Log(creepsAgent.remainingDistance);
        if (creepsAgent.remainingDistance <= creepsAgent.stoppingDistance)
        {
            
            nextRouteID++;
            //creepsAgent.SetDestination(towers[nextRouteID].transform.position);
            creepsAgent.SetDestination(routes[nextRouteID].transform.position);
            if (nextRouteID >= threshold)
            {
                towerMovementMode = true;
            }
        }

    }
    //Checks if any unit enters the creep's collider range
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Fraction>() == null) return;

        //Attack the object if it is not an ally
        if (other.GetComponent<Fraction>().userFraction != this.GetComponent<Fraction>().userFraction)
        {
            status = creepAIStatus.attacking;
            target = other.gameObject;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Switch target to null if the target left the creep's range
        if (target == other.gameObject)
        {
            target = null;
            //status = creepAIStatus.moving;
        }
    }

    //move onto the next tower
    void switchTower()
    {
        if (nextTowerId != towers.Length)
        {
            nextTowerId++;
            switchTargettoTower();
            creepsAgent.SetDestination(target.transform.position);
        }
        
    }

    void switchTargettoTower()
    {
        target = towers[nextTowerId];
    }
    //this is where the enemy would go to a specific tower
    void moveEnemyToTower()
    {
        creepsAgent.SetDestination(towers[nextTowerId].transform.position);
    }

    //this is where to setup the tower list
    public void setupTowerTargetList(GameObject[] list)
    {
        charFraction fraction = this.GetComponent<Fraction>().userFraction;

        towers = list;
        //for (int i = 0; i < list.Length; i++)
        //{
        //    towers[i] = list[i];
        //}
    }

    public void setupPathingList(GameObject[] list)
    {
        charFraction fraction = this.GetComponent<Fraction>().userFraction;
        routes = list;

    }

    void attackTarget()
    {

    }
}
