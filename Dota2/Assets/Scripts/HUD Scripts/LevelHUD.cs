﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelHUD : MonoBehaviour
{
    public UserExpirence levelRef;
    [SerializeField] private Image currentLevelImg;
    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        text = GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update()

    {
        //currentLevelImg.fillAmount = Mathf.Clamp(levelRef.getNormalizedEXP(), 0, 1);
        text.text = levelRef.level.ToString();
    }
}
