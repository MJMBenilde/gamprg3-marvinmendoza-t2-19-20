﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Health healthRef;

    [SerializeField] private Image currentHealthImg;
    private Text text;

    private void Start()
    {
        text = GetComponentInChildren<Text>();
    }

    void Update()
    {
        currentHealthImg.fillAmount = Mathf.Clamp(healthRef.GetNormalizedHealth(), 0, 1);
        text.text = healthRef.CurrentHealth + "/" + healthRef.MaxHealth;
    }
}
