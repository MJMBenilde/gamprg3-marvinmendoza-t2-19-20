﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PlayerScoreInfo
{
    public GameObject playerData;
    public string playerName;
    public int kill;
    public int death;
    public int assist;
    public int gold;
    public int totalGold;
    public int level;

}
[System.Serializable]
public class UIPlayerScoreInfo  
{
    public GameObject scoreBoard;
    public Text playerNameUI;
    public Text killUI;
    public Text deathUI;
    public Text assistUI;
    public Text goldUI;
    public Text levelUI;
}
public class ScoreBoardHUD : MonoBehaviour
{
    //public PlayerManager playerManager;
    public PlayerManager playerManager;

    [SerializeField] public UIPlayerScoreInfo[] direScoreBoard;
    [SerializeField] public UIPlayerScoreInfo[] radiantScoreBoard;

    [SerializeField] public PlayerScoreInfo[] direTeam;
    [SerializeField] public PlayerScoreInfo[] radiantTeam;
    // Start is called before the first frame update
    void Start()
    {
        //SetPlayerData();
    }

    // Update is called once per frame
    void Update()
    {
        GetDireTeamStats();
        GetRadiantTeamStats();
        UpdateEntireScoreboard();
    }

    void GetDireTeamStats()
    {
        for (int i = 0; i < direTeam.Length; i++)
        {
            direTeam[i].kill = direTeam[i].playerData.GetComponent<ScoreStats>().killpoint;
            direTeam[i].death = direTeam[i].playerData.GetComponent<ScoreStats>().deathlpoint;
            direTeam[i].gold = direTeam[i].playerData.GetComponent<ScoreStats>().goldlpoint;
            direTeam[i].totalGold = direTeam[i].playerData.GetComponent<ScoreStats>().totalGoldPoint;
            direTeam[i].level = direTeam[i].playerData.GetComponent<ScoreStats>().levellpoint;
        }
    }

    void GetRadiantTeamStats()
    {
        for (int i = 0; i < radiantTeam.Length; i++)
        {
            radiantTeam[i].kill = radiantTeam[i].playerData.GetComponent<ScoreStats>().killpoint;
            radiantTeam[i].death = radiantTeam[i].playerData.GetComponent<ScoreStats>().deathlpoint;
            radiantTeam[i].gold = radiantTeam[i].playerData.GetComponent<ScoreStats>().goldlpoint;
            radiantTeam[i].totalGold = radiantTeam[i].playerData.GetComponent<ScoreStats>().totalGoldPoint;
            radiantTeam[i].level = radiantTeam[i].playerData.GetComponent<ScoreStats>().levellpoint;
        }
    }

    void UpdateEntireScoreboard()
    {
        //dire
        for (int i = 0; i < direTeam.Length; i++)
        {
            direScoreBoard[i].killUI.text = direTeam[i].kill.ToString();
            direScoreBoard[i].levelUI.text = direTeam[i].kill.ToString();
            direScoreBoard[i].deathUI.text = direTeam[i].death.ToString();
            direScoreBoard[i].goldUI.text = direTeam[i].gold.ToString() + "/" + direTeam[i].totalGold.ToString();
            direScoreBoard[i].levelUI.text = direTeam[i].level.ToString();
        }

        //radiant
        for (int i = 0; i < radiantTeam.Length; i++)
        {
            radiantScoreBoard[i].killUI.text = radiantTeam[i].kill.ToString();
            radiantScoreBoard[i].levelUI.text = radiantTeam[i].kill.ToString();
            radiantScoreBoard[i].deathUI.text = radiantTeam[i].death.ToString();
            radiantScoreBoard[i].goldUI.text = radiantTeam[i].gold.ToString() + "/" + radiantTeam[i].totalGold.ToString();
            radiantScoreBoard[i].levelUI.text = radiantTeam[i].level.ToString();
        }


    }

    void SetPlayerData()
    {
        //dire Team

        for (int i = 0; i < playerManager.direTeamList.Length; i++)
        {
            direTeam[i].playerData = playerManager.direTeamList[i];
        }

        //radiant Team
        for (int i = 0; i < playerManager.radiantTeamList.Length; i++)
        {
            radiantTeam[i].playerData = playerManager.radiantTeamList[i];
        }
    }

}
