﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillStreaker : MonoBehaviour
{
    public int currentKillstreak;
    public int killAmount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //use if you kill
    public void raiseKS()
    {
        currentKillstreak++;
    }

    //use if Dead
    public void resetKS()
    {
        currentKillstreak = 0;
    }

    public int gettotalKills()
    { return killAmount; }
}
