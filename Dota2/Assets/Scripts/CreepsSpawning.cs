﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.AI;
public enum aiPosition
{
    top,
    mid,
    bottom
}

public class CreepsSpawning : MonoBehaviour
{
    //this link to the spawn manager components
    public SpawnManager spawnManager;

    public UnityEvent spawn;
     
    public bool activateSecondLevel;

    //public GameObject[] creeppool;

    //public GameObject[] currentCreepSpawn; //creeps to spawn

    public int totalSpawned; //manages the total spawned creeps - will be use to add more creeps
    public int spawnAmount; //see how many wave have spawned before the siege
    public int waveSpawn; //manages the max 10th wave
    public GameObject[] towerList;
    public GameObject[] pathList;
    public float timer = 30.0f;
    public aiPosition position;

    public int noOfMeele;
    public int noOfRanged;
    public int noOfSiege;

    //growth
    public int growthAdditional = 15; //raise every 15 wave
    public int growthLimit; //current amount;
    public int health;
    public int attackDamage;
    public int goldBounty;
    public int expirenceBounty;

    public int curhealth;
    public int curattackDamage;
    public int curgoldBounty;
    public int curexpirenceBounty;

    // Start is called before the first frame update
    void Start()
    {
        spawn.AddListener(UponDestroy);
        //currentCreepSpawn = spawnManager.generalCreepSpawn;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            ResetTimer();
            SpawnEnemy(this.GetComponent<Fraction>().userFraction, this.gameObject);
        }
    }

    void UponDestroy()
    {

    }

    void ResetTimer()
    {
        timer = spawnManager.spawnInterval;
    }

    void SpawnEnemy(charFraction fraction, GameObject spawner)
    {
        spawnAmount++;
        totalSpawned++;
        growthLimit++;
        //check if raising stats now
        //if (growthLimit >= growthAdditional)
        //{
        //    curhealth += health;
        //    curattackDamage += attackDamage;
        //    curgoldBounty += goldBounty;
        //    curexpirenceBounty += expirenceBounty;
        //}

        //if (spawnAmount >= waveSpawn)
        //{
        //    if (activateSecondLevel == true)
        //    {
        //        currentCreepSpawn = spawnManager.superSiegeCreepSpawn;
        //    }

        //    else
        //    {
        //        currentCreepSpawn = spawnManager.siegeCreepSpawn;
        //    }
        //    //resets to zero
        //    spawnAmount = 0;
        //}

        //// spawning Meele creeps
        for (int i = 0; i < noOfMeele; i++)
        {
            GameObject newCreep;
            if (activateSecondLevel == false)
            {
                newCreep = Instantiate(spawnManager.generalCreep);
            }
            else
            {
                newCreep = Instantiate(spawnManager.superGeneralCreep);
            }
            //startImprovements(newCreep);
            newCreep.GetComponent<Fraction>().setFraction(fraction);
            newCreep.GetComponent<Fraction>().setFractionColor();

            //set AI Targeting Lists
            newCreep.GetComponent<CreepsAI>().setupTowerTargetList(towerList);
            newCreep.GetComponent<CreepsAI>().setupPathingList(pathList);
            newCreep.gameObject.transform.position = spawner.gameObject.transform.position;
        }

        //spawn Ranged Creep
        for (int i = 0; i < noOfRanged; i++)
        {
            GameObject newCreep;
            if (activateSecondLevel == false)
            {
                newCreep = Instantiate(spawnManager.rangedCreep);
            }
            else
            {
                newCreep = Instantiate(spawnManager.superRangedCreep);
            }
            //startImprovements(newCreep);
            newCreep.GetComponent<Fraction>().setFraction(fraction);
            newCreep.GetComponent<Fraction>().setFractionColor();

            //set AI Targeting Lists
            newCreep.GetComponent<CreepsAI>().setupTowerTargetList(towerList);
            newCreep.GetComponent<CreepsAI>().setupPathingList(pathList);
            newCreep.gameObject.transform.position = spawner.gameObject.transform.position;
        }

        if (spawnAmount >= waveSpawn)
        {
            //spawn Siege Creep
            for (int i = 0; i < noOfSiege; i++)
            {
                GameObject newCreep;
                if (activateSecondLevel == false)
                {
                    newCreep = Instantiate(spawnManager.rangedCreep);
                }
                else
                {
                    newCreep = Instantiate(spawnManager.superRangedCreep);
                }
                //startImprovements(newCreep);
                newCreep.GetComponent<Fraction>().setFraction(fraction);
                newCreep.GetComponent<Fraction>().setFractionColor();

                //set AI Targeting Lists
                newCreep.GetComponent<CreepsAI>().setupTowerTargetList(towerList);
                newCreep.GetComponent<CreepsAI>().setupPathingList(pathList);
                newCreep.gameObject.transform.position = spawner.gameObject.transform.position;
            }
            //reset Spawner Amount
            spawnAmount = 0;
        }

        //set next batch of creeps;
        SetNextCreepSpawn();

    }

    //this setup the next creep wave
    void SetNextCreepSpawn()
    {

    }

    public void enableSecondLevelMode()
    {
        activateSecondLevel = true;
        Debug.Log(this.gameObject.name + ": Second Level have enable for this spawner");
    }

    //this handles improvements of creeps
    public void startImprovements(GameObject creep)
    {
        //creep.GetComponent<Health>().MaxHealth += health;
        creep.GetComponent<AttackComponent>().Damage += attackDamage;
        creep.GetComponent<GoldSender>().goldToSend += goldBounty;
        creep.GetComponent<ExpSender>().expirenceAmount += expirenceBounty;
    }
}
