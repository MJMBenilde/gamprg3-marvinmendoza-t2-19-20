﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldManager : MonoBehaviour
{
    public GameObject[] heroList;

    public GameObject[] redheroList;
    public GameObject[] blueheroList;
    // Start is called before the first frame update
    void Start()
    {
        GiveInitialGold();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void GiveInitialGold()
    {
        for (int i = 0; i < heroList.Length; i++)
        {
            heroList[i].GetComponent<Gold>().gainGold(600);
        }
    }

    public void GrantGoldToRedTeam(int goldAmount)
    {
        for (int i = 0; i < redheroList.Length; i++)
        {
            redheroList[i].GetComponent<Gold>().gainGold(goldAmount);
        }

    }

    public void GrantGoldToBlueTeam(int goldAmount)
    {
        for (int i = 0; i < blueheroList.Length; i++)
        {
            blueheroList[i].GetComponent<Gold>().gainGold(goldAmount);
        }

    }
}
