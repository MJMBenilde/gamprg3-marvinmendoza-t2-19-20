﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fountain : MonoBehaviour
{

    //use for regening players;

    // Start is called before the first frame update

    [SerializeField] private List<GameObject> alliesInRadius = new List<GameObject>();
    public float AuraRange = 70;
    private Fraction fraction;

    void Start()
    {
        fraction.GetComponent<Fraction>();
    }

    // Update is called once per frame
    void Update()
    {
        if (alliesInRadius.Count > 0)
        {
            foreach (GameObject ally in alliesInRadius)
            {
                if (ally == null) //removes destroyed allies from the list
                {
                    alliesInRadius.Remove(ally);
                    continue;
                }
                if (Vector3.Distance(ally.transform.position, this.transform.position) <= AuraRange)
                {
                    //Buff
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Fraction>() == null) return;

        //If the target is an ally and is within aura range
        else if (other.GetComponent<Fraction>().userFraction == fraction.userFraction)
        {
            alliesInRadius.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (alliesInRadius.Contains(other.gameObject)) alliesInRadius.Remove(other.gameObject);
    }
}
