﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Ancient : Structure
{
    public int invunablelock = 2;
    public UnityEvent postDestructionSetup;

    private void Start()
    {
        health = GetComponent<Health>();
    }

    private void Update()
    {
        if (health.CurrentHealth <= 0)
        {
            destroyed();
        }
    }


    public void removeSpecialInvunability()
    {
        invunablelock--;

        if (invunablelock <= 0)
        {
            this.RemoveVulnerability();
        }
    }

    public void destroyed()
    {
        postDestructionSetup.Invoke();
        Destroy(this.gameObject);
    }
}
