﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Tower : Structure
{
    [Header("Tower Radius")]
    public SphereCollider TowerRadius;

    [Header("Combat")]
    public float AttackRange = 70;
    public float AuraRange = 70;

    public int TowerTier { get => towerTier; }

    [Range(1, 4)]
    [SerializeField] private int towerTier = 1;
    private RangedAttack attack;

    [SerializeField] private List<GameObject> enemiesInRadius = new List<GameObject>();
    [SerializeField] private List<GameObject> alliesInRadius = new List<GameObject>();

    private Fraction fraction;
    public float healthPercentage;
    public float mpPercentage;
    public UnityEvent removeVunalibilityOfNextTower;
    public UnityEvent preDestruction;

    public bool fountain;

    private void Start()
    {
        health = GetComponent<Health>();
        attack = GetComponent<RangedAttack>();

        fraction = GetComponent<Fraction>();

        TowerRadius.isTrigger = true;

        //ApplyTierBuffs();
    }

    private void Update()
    {
        //Check enemies within tower radius
        if (enemiesInRadius.Count > 0)
        {
            foreach (GameObject enemy in enemiesInRadius)
            {
                if (enemy == null) //removes destroyed enemies from the list
                {
                    enemiesInRadius.Remove(enemy);
                    continue;
                }

                if (Vector3.Distance(enemy.transform.position, this.transform.position) <= AttackRange)
                {
                    attack.Attack(enemy.GetComponent<Health>());
                    break;
                }
            }
        }

        //Check allies within tower radius
        if (alliesInRadius.Count > 0)
        {
            foreach (GameObject ally in alliesInRadius)
            {
                if (ally == null) //removes destroyed allies from the list
                {
                    alliesInRadius.Remove(ally);
                    continue;
                }
                if (Vector3.Distance(ally.transform.position, this.transform.position) <= AuraRange)
                {
                    //Buff
                    fountainRegeneration(ally);
                }
            }
        }

        if (health.CurrentHealth <= 0)
        {
            TowerDestruction();
        }
    }

    /// <summary>
    /// Modifies components based on the tower tier
    /// </summary>
    private void ApplyTierBuffs()
    {
        attack.Damage *= towerTier; //temp values
        attack.BaseAttackTime /=  towerTier; //temp values
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Fraction>() == null) return;

        //If the target is not an ally and is within attack range
        if (other.GetComponent<Fraction>().userFraction != fraction.userFraction)
        {
            enemiesInRadius.Add(other.gameObject);
        }

        //If the target is an ally and is within aura range
        else if (other.GetComponent<Fraction>().userFraction == fraction.userFraction)
        {
            alliesInRadius.Add(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Remove objects from the lists once they move out of range
        if (enemiesInRadius.Contains(other.gameObject)) enemiesInRadius.Remove(other.gameObject);

        if (alliesInRadius.Contains(other.gameObject)) alliesInRadius.Remove(other.gameObject);
    }

    public void TowerDestruction()
    {
        removeVunalibilityOfNextTower.Invoke();
        preDestruction.Invoke();
        this.gameObject.transform.position = new Vector3(9999, 9999, 9999);
        Destroy(this.gameObject);
    }

    public void fountainRegeneration(GameObject ally)
    {
        if (ally.gameObject.tag != "player") return;

        if (fountain == true)
        {
            Debug.Log("REGENERATING");
            //get the 5% of player's health
            float healthTotalPercentage = healthPercentage * ally.gameObject.GetComponent<Health>().MaxHealth;
            Debug.Log("REGENERATING: " + healthTotalPercentage);
            float mpTotalPercentage = mpPercentage * ally.gameObject.GetComponent<Health>().MaxHealth;
            Debug.Log("REGENERATING: " + mpTotalPercentage);
            ally.gameObject.GetComponent<Health>().healthRegen(healthTotalPercentage);
            ally.gameObject.GetComponent<Mana>().manaRegen(mpTotalPercentage);
        }
    }

}
