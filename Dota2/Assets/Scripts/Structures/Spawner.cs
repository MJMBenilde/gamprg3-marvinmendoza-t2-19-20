﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Spawner : Structure
{
    public UnityEvent ifDestroyed;
    // Start is called before the first frame update
    void Start()
    {
        health = GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        if (health.CurrentHealth <= 0)
        {
                SpawnerDestruction();
        }
    }

    public void SpawnerDestruction()
    {
        this.GetComponent<Spawner>().ifDestroyed.Invoke();
        Destroy(this.gameObject);
    }
}
