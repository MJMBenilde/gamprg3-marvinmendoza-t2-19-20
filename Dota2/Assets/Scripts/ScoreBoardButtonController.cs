﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreBoardButtonController : MonoBehaviour
{
    public GameObject scoreboard;
    public Button uIButton;

    public GameObject[] disableOthers;
    public bool Disableotheritemsbool;
    public bool scoreboardEnable;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisableOthersItems()
    {
        if (Disableotheritemsbool == true)
        {
            Disableotheritemsbool = false;
        }
        else
        {
            Disableotheritemsbool = true;
        }
        for (int i = 0; i < disableOthers.Length; i++)
        {
            disableOthers[i].SetActive(Disableotheritemsbool);
        }
    }

    public void EnableScoreBoard()
    {
        if (scoreboardEnable == true)
        {
            scoreboardEnable = false;
        }
        else
        {
            scoreboardEnable = true;
        }

        scoreboard.SetActive(scoreboardEnable);
    }
}
