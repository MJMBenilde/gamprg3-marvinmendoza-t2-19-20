﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Clocksystemv2 : MonoBehaviour
{
    public int hours;
    public float minute;
    public int days;
    public Text physicalClock;

    public float maxTime = 10;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        minute += Time.deltaTime;

        if (minute >= 60)
        {
            hours++;
            minute = 0;
        }

        if (hours >= maxTime)
        {
            days++;
            hours = 0;
        }

        physicalClock.text = "Time: " + days + ":" + hours + ":" + minute;
    }
}
