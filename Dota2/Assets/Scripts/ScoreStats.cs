﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreStats : MonoBehaviour
{

    public int killpoint;
    public int deathlpoint;
    public int assistlpoint;
    public int goldlpoint;
    public int totalGoldPoint;
    public int levellpoint;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        getUserGold();
        getUserLevel();
        raiseDeathStats();
        raiseKillStats();
    }

    public void getUserGold()
    {
        goldlpoint = this.gameObject.GetComponent<Gold>().gold;
    }

    public void getUserLevel()
    {
        levellpoint = this.gameObject.GetComponent<UserExpirence>().level; 
    }

    public void raiseDeathStats()
    {
        deathlpoint = this.gameObject.GetComponent<Health>().deathAmount;
    }

    public void raiseKillStats()
    {
        killpoint = this.gameObject.GetComponent<KillStreaker>().killAmount;
    }

}
