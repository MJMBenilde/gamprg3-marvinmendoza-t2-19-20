﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class CharacterMovement : MonoBehaviour
{
    public NavMeshAgent playerAgent;

    public GameObject player;

    public GameObject movementIndicator;
    public GameObject enemyTarget;

    public Vector3 characterTarget;

    public bool moving;
    public bool attacking;
    public bool stopping;

    public Stats playerStats;
    // Start is called before the first frame update
    void Start()
    {
        playerAgent = this.GetComponent<NavMeshAgent>();
        playerStats = this.GetComponent<Stats>();

        //initial configurate movement speed;
        UpdateSpeed();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateSpeed();
        if (enemyTarget != null)
        {
            playerAgent.SetDestination(enemyTarget.gameObject.transform.position);
        }

        if (attacking == true)
        {
            attackingStoppingMovement();
        }

        if (Input.GetButtonDown("Fire2"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;

            Physics.Raycast(ray, out hit);

            
            if (hit.collider.gameObject.tag == "creep" && hit.collider.GetComponent<Fraction>() != null && this.gameObject.GetComponent<Fraction>().userFraction != hit.collider.GetComponent<Fraction>().userFraction)
            {
                Debug.Log("Enemy Selected");
               movementIndicator.SetActive(true);
               enemyTarget = hit.collider.gameObject;
               movementIndicator.GetComponent<Rigidbody>().position = new Vector3(enemyTarget.gameObject.transform.position.x, 0.0f, enemyTarget.gameObject.transform.position.z);
                //set target to be attackable
               this.gameObject.GetComponent<PlayerMeleeAttack>().target = hit.collider.gameObject;
               playerAgent.SetDestination(new Vector3(enemyTarget.gameObject.transform.position.x, 0.0f, enemyTarget.gameObject.transform.position.z));
                attacking = true;
               attackingStoppingMovement();
            }
            else if (hit.collider.gameObject.tag == "tower" && hit.collider.GetComponent<Fraction>() != null && this.gameObject.GetComponent<Fraction>().userFraction != hit.collider.GetComponent<Fraction>().userFraction)
            {

                    Debug.Log("Enemy Selected");
                    movementIndicator.SetActive(true);
                    enemyTarget = hit.collider.gameObject;
                    movementIndicator.GetComponent<Rigidbody>().position = new Vector3(enemyTarget.gameObject.transform.position.x, 0.0f, enemyTarget.gameObject.transform.position.z);
                    playerAgent.SetDestination( new Vector3(enemyTarget.gameObject.transform.position.x,0.0f, enemyTarget.gameObject.transform.position.z));
                attacking = true;
                attackingStoppingMovement();

            }
            else
            {
                movementIndicator.SetActive(true);
                enemyTarget = null;
                attacking = false;
                playerAgent.isStopped = false;
                movementIndicator.GetComponent<Rigidbody>().position = new Vector3(hit.point.x, 0.00f, hit.point.z);
                playerAgent.SetDestination(new Vector3(hit.point.x,0.00f, hit.point.z));
            }

            Debug.Log(hit.point);
        }

        if (playerAgent.remainingDistance >= playerAgent.stoppingDistance || stopping == true)
        {
            moving = true;
        }

        else
        {
            movementIndicator.SetActive(false);
            moving = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "creep")
        {
            Debug.Log("Enemy Spotted");
        }
        if (other.gameObject == enemyTarget.gameObject)
        {
            Debug.Log("Attacking: " + other.gameObject.name);
            //start attacking
            this.GetComponent<AttackComponent>().Attack(enemyTarget.GetComponent<Health>());
        }
    }

    void stoppingMovement()
    {
        if (playerAgent.remainingDistance <= playerAgent.stoppingDistance)
        {
            stopping = true;
            playerAgent.isStopped = true;
        }
    }
    void attackingStoppingMovement()
    {
        if (playerAgent.remainingDistance <= playerAgent.stoppingDistance + 3)
        {
            stopping = true;
            playerAgent.isStopped = true;
        }
    }

    public void targetDefeated()
    {
        stopping = false;
        playerAgent.isStopped = false;
    }

    public void targetEscaping()
    {
        //resumemoving 
        stopping = false;
        playerAgent.isStopped = false;
        //new active target
        playerAgent.SetDestination(new Vector3(enemyTarget.gameObject.transform.position.x, 0.0f, enemyTarget.gameObject.transform.position.z));
    }

    public void UpdateSpeed()
    {
        playerAgent.speed = playerStats.movementSpeed;
        playerAgent.acceleration = playerStats.movementSpeed;
    }
}
