﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Buff/Mana Regen")]
public class Buff_ManaRegen : Buff
{
    public float RegenPerTick = 0.1f;

    protected override void OnBuffTick(BuffReceiver target)
    {
        Debug.Log("mana regen");
        target.Mana.gainMana(RegenPerTick);

        //Code for regen cancel on damage
    }
}
