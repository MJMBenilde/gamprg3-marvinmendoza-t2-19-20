﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BuffStackType
{
    EffectStackable, //Buff effect intensifies per new stack
    EffectRefresh, //Buff effect refreshes duration on new stack
    NotStackable //yeah
}

//public enum BuffEffectType
//{
//    Positive, Negative, Mixed
//}

public class Buff : ScriptableObject
{
    [Header("Buff Identity")]
    public string BuffName;

    [Header("Buff Lifetime")]
    public BuffStackType StackType;
    public bool InfiniteDuration;
    public float Duration;
    [HideInInspector] public float Lifetime = 0;

    [Header("Buff Effects")]
    public float BuffInterval;
    [HideInInspector] public uint CurrentStacks = 1;
    //

    public void StartBuff(BuffReceiver target)
    {
        OnBuffStart(target);
    }

    public void UpdateBuff(BuffReceiver target)
    {
        OnBuffTick(target);
    }

    public void EndBuff(BuffReceiver target)
    {
        OnBuffEnd(target);
    }

    //Buff effect overrides
    #region
    protected virtual void OnBuffStart(BuffReceiver target) { }
    protected virtual void OnBuffTick(BuffReceiver target) { }
    protected virtual void OnBuffEnd(BuffReceiver target) { }
    #endregion
}
