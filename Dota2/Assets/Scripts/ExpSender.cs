﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpSender : MonoBehaviour
{
    public GameObject[] targets;
    public int expirenceAmount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void playerEXP(int amount)
    {
        if (this.gameObject.GetComponent<UserExpirence>() == null) return;

    }
    public void splitEXP()
    {
        float newExpAmount;
       //if player attacks
        if (this.gameObject.tag == "player")
        {
            newExpAmount = (40 + 0.13f * this.gameObject.GetComponent<UserExpirence>().getLevelBounty()) / targets.Length;
        }

        else
        {
            newExpAmount = expirenceAmount / targets.Length;
        }
            
            for (int i = 0; i < targets.Length; i++)
            {
            targets[i].GetComponent<UserExpirence>().gainEXP((int)newExpAmount);
            }
    }

    //kill range

}
