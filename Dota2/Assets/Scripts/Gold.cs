﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Gold : MonoBehaviour
{
    public int gold;
    public int totalGold;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    //use this after defeat
    public void gainGold(int amount)
    {
        gold += amount;
        totalGold += amount;
    }

    //use this on a market script
    public bool spendGold(int payment)
    {
        if (gold <= payment)
        {
            Debug.Log("Can't purchase item");
            return false;
        }
        else
        {
            return true;
        }

    }

    public void useGold(int amount)
    {
        gold -= amount;
    }
}
